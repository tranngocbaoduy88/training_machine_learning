#!/usr/bin/env python
# coding: utf-8

# In[8]:


import matplotlib.pyplot as plt
import numpy as np


# In[9]:


def forward(x, w, b):
    return x * w + b

def loss(x, w, y):
    return (x * w - y) ** 2

f = open("data.txt", "r")
x = list()
y = list()
y1 = list()
y2 = list()
y3 = list()

for i in range(3):
    row = f.readline()
    a = row.split()
    x.append(int(a[0]))
    y.append(int(a[1]))
f.close()
 
for i in range(3): 
    y1.append(forward(x[i],2,0))
    y2.append(forward(x[i],3,0))
    y3.append(forward(x[i],4,0))

plt.plot(x,y1,'r-')
plt.plot(x,y2,'b-')
plt.plot(x,y3,'g-')
plt.show()


# In[40]:


import numpy as np

def forward(x, w):
    return x * w

def loss(x, y, w):
    return (forward(x, w)- y) ** 2

def aveList(arr):
    return sum(arr)/ len(arr)

#read data from file
f = open("data.txt", "r")
x = list()
y = list() 

for i in range(3):
    row = f.readline()
    a = row.split()
    x.append(int(a[0]))
    y.append(int(a[1]))
f.close()
 

loss_list = list()
w_list = list()

for w in np.arange(-15,19,0.01):
    val_list = list()
    for i in range(3):  
        val_list.append(loss(x[i],y[i],w)) 
    loss_list.append(aveList(val_list))
    w_list.append(w)

plt.plot(w_list,loss_list,'g-')
plt.ylabel('Loss')
plt.xlabel('w')
plt.show()

