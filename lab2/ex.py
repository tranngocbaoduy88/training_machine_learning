import matplotlib.pyplot as plt
import numpy as np

def forward(x, w):
    return x * w

def loss(x, y, w):
    return (forward(x, w)- y) ** 2

def aveList(arr):
    return sum(arr)/ len(arr)

def calGradient(x, y, w):
    return 2 * x * (x * w -y)

w = 1.0

#read data from file
f = open("data.txt", "r")
x_list = list()
y_list = list() 

for i in range(3):
    row = f.readline()
    a = row.split()
    x_list.append(int(a[0]))
    y_list.append(int(a[1]))
f.close()
 
print("predict (before training)", "4 hours", forward(4, w))
w = 1.0
for epoch in range(10):
    for x,y in zip(x_list, y_list): 
        grad = calGradient(x, y, w)
        print("\tgrad: ", x, y, w, grad) 
        w = w - 0.01 * grad
        l = loss(x, y, w)
    print("progress:", epoch, "w=", w, "loss=", l)
    
print("predict (after training)", "4 hours", forward(4, w))


def forward(x, w1, w2):
    return x**2* w2 + x*w1 +3

def loss(x, y, w1, w2):
    return (forward(x, w1, w2)- y) *(forward(x, w1, w2)- y)
 
def calGradientW1(x, y, w1, w2):
    return 2 * (x **2 )* (w1* (x**2) - w2*x  - y)

 
def calGradientW2(x, y, w1, w2):
    return 2 * x * (w1* (x**2) - w2*x  - y)


w1 = 1.0
w2 = 1.0

print("predict (before training)", "4 hours", forward(4, w1, w2))

loss_list = list()
w1_list = list()
w2_list = list()

for epoch in range(100):
    w1_list.append(w1)
    w2_list.append(w2)
    for x,y in zip(x_list, y_list): 
        grad1 = calGradientW1(x, y, w1, w2)
        grad2 = calGradientW2(x, y, w1, w2)
        print("\tgrad: ", x, y, grad1, grad2)  
        w1 = w1 - 0.01 * grad1
        w2 = w2 - 0.01 * grad2
        l = loss(x, y, w1, w2)
    loss_list.append(l / 3)
    print("progress:", epoch, "w1=", w1, "w2=", w2, "loss=", l)
    
print("predict (after training)", "4 hours", forward(4, w1, w2))


plt.plot(w1_list,loss_list,'g-')
plt.plot(w2_list,loss_list,'r-')
plt.ylabel('Loss')
plt.xlabel('w')
plt.show()